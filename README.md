Key Files:

* BaselineAlgorithm.py - Runs HITS on the general dataset 
* AccuracyWeightAlgorithm.py - Runs an accuracy weighted Hits Algorithm
* HeatMapGen.py - Generates JavaScript Code component for Google Maps API
* InflectionDetect.py - Analysis of Restaurant Review rates
* AggregateInflection.py - Charts related to review rates
* CompareExperts.py - Evaluate Results of Various models with different group sizes.